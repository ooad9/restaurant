import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Employee from "@/types/Employee";
import employeeService from "@/services/employee";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useEmployeeStore = defineStore("Employee", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const dialog = ref(false);
  const employees = ref<Employee[]>([]);
  const editedEmployee = ref<Employee>({
    firstname: "",
    lastname: "",
    username: "",
    password: "",
    age: 0,
    phone: "",
    position: "",
  });

  watch(dialog, (newDialog, olDialog) => {
    if (!newDialog) {
      editedEmployee.value = {
        firstname: "",
        lastname: "",
        username: "",
        password: "",
        age: 0,
        phone: "",
        position: "",
      };
    }
  });

  async function getEmployees() {
    loadingStore.isLoading = true;
    try {
      const res = await employeeService.getEmployees();
      employees.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึง Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveEmployee() {
    loadingStore.isLoading = true;
    try {
      if (editedEmployee.value.id) {
        await employeeService.updateEmployee(
          editedEmployee.value.id,
          editedEmployee.value
        );
      } else {
        await employeeService.saveEmployee(editedEmployee.value);
      }

      dialog.value = false;
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึก Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  function editEmployee(product: Employee) {
    editedEmployee.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  async function deleteEmployee(id: number) {
    loadingStore.isLoading = true;
    try {
      await employeeService.deleteEmployee(id);
      await getEmployees();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบ Employee ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    employees,
    getEmployees,
    dialog,
    editedEmployee,
    saveEmployee,
    deleteEmployee,
    editEmployee,
  };
});
