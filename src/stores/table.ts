import type Table from "@/types/Table";
import { defineStore } from "pinia";
import { ref, watch } from "vue";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import tableService from "@/services/table";

export const useTableStore = defineStore("Table", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  const dialog = ref(false);
  const tables = ref<Table[]>([]);
  const editedTable = ref<Table>({
    id: 0,
    name: "",
    amount: 0,
    status: "",
  });

  watch(dialog, (newDialog, olDialog) => {
    if (!newDialog) {
      editedTable.value = {
        id: 0,
        name: "",
        amount: 0,
        status: "",
      };
    }
  });

  async function getTables() {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.getTables();
      tables.value = res.data;
    } catch (e) {
      console.log(e);
      //   messageStore.showError("ไม่สามารถดึง Table ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveTable() {
    loadingStore.isLoading = true;
    try {
      if (editedTable.value.id) {
        await tableService.updateTable(editedTable.value.id, editedTable.value);
      } else {
        await tableService.saveTable(editedTable.value);
      }

      dialog.value = false;
      await getTables();
    } catch (e) {
      console.log(e);
      //   messageStore.showError("ไม่สามารถบันทึก Table ได้");
    }
    loadingStore.isLoading = false;
  }

  function editTable(product: Table) {
    editedTable.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  async function deleteTable(id: number) {
    loadingStore.isLoading = true;
    try {
      await tableService.deleteTable(id);
      await getTables();
    } catch (e) {
      console.log(e);
      //   messageStore.showError("ไม่สามารถลบ Table ได้");
    }
    loadingStore.isLoading = false;
  }

  return {
    tables,
    getTables,
    dialog,
    editedTable,
    saveTable,
    deleteTable,
    editTable,
  };
});
