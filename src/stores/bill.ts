import { ref } from "vue";
import { defineStore } from "pinia";
import type Bill from "@/types/Bill";

export const useBill = defineStore("Bill", () => {
  const bill = ref<Bill[]>([
    { id: 1, queue: 1, table: 1, menu: "ข้าวกะเพราหมู", qty: 1, price: 45 },
    { id: 2, queue: 2, table: 1, menu: "ต้มยำทะเล", qty: 1, price: 60 },
    { id: 3, queue: 3, table: 1, menu: "ข้าวผัดหมู", qty: 1, price: 45 },
  ]);

  const bill2 = ref<Bill[]>([
    { id: 10, queue: 1, table: 1, menu: "หมูๆ", qty: 1, price: 20 },
    { id: 20, queue: 2, table: 1, menu: "เลๆ", qty: 1, price: 10 },
    { id: 30, queue: 3, table: 1, menu: "ข้าวผัดหมู", qty: 1, price: 35 },
  ]);

  return {
    bill,
    bill2,
  };
});
