import { ref } from "vue";
import { defineStore } from "pinia";
//import { mdiTagPlusOutline } from "@mdi/js";

export enum DialogType {
  info,
  error,
  confirm,
}

export const useMessageStore = defineStore("counter", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeout = ref(2000);
  const type = ref<DialogType>(DialogType.info);

  const showMessage = (msg: string, tout: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeout.value = tout;
  };
  function showError(text: string) {
    message.value = text;
    isShow.value = true;
  }
  const closeMessage = () => {
    message.value = "";
    isShow.value = false;
  };

  return { isShow, message, showMessage, closeMessage, timeout , showError };
});
