import { ref } from "vue";
import { defineStore } from "pinia";
import type Serve from "@/types/Serve";

export const useServeStore = defineStore("serve", () => {
  const isTable = ref(true);
  const dialog = ref(false);
  let i = 0;
  let lastId = 3;
  const serves = ref<Serve[]>([
    { id: 1, queue: 1, table: 1, menu: "ข้าวกะเพราหมู", qty: 1, status: "" },
    { id: 2, queue: 2, table: 1, menu: "ต้มยำทะเล", qty: 1, status: "" },
    { id: 3, queue: 3, table: 1, menu: "ข้าวผัดหมู", qty: 1, status: "" },
  ]);
  const close = () => {
    dialog.value = false;
  };
  const editedServe = ref<Serve>({
    id: -1,
    queue: -1,
    table: -1,
    menu: "",
    qty: -1,
    status: "",
  });
  const deleteServe = () => {
    const index = serves.value.findIndex(
      (item) => item.id === editedServe.value.id
    );
    editedServe.value.status = "เสิร์ฟแล้ว";
    serves.value.splice(index, 1);
    dialog.value = false;
  };
  const editServe = (serve: Serve) => {
    editedServe.value = { ...serve };
    dialog.value = true;
  };
  const saveServe = () => {
    if (editedServe.value.id < 0) {
      editedServe.value.id = lastId++;
      serves.value.push(editedServe.value);
    } else {
      const index = serves.value.findIndex(
        (item) => item.id === editedServe.value.id
      );
      editedServe.value.status = "เสิร์ฟแล้ว";
      i++;
      if (i == serves.value.length) {
        deleteServe();
      }

      serves.value[index] = editedServe.value;
    }
    dialog.value = false;
  };
  const clear = () => {
    editedServe.value = {
      id: -1,
      queue: -1,
      table: -1,
      menu: "",
      qty: -1,
      status: "",
    };
  };
  return {
    isTable,
    serves,
    dialog,
    editedServe,
    close,
    deleteServe,
    editServe,
    saveServe,
    clear,
  };
});
