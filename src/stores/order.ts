import type Order from "@/types/Order";
import { defineStore } from "pinia";
import { computed, ref, watch } from "vue";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import orderService from "@/services/order";
import type orderItem from "@/types/OrderItem";
import type Food from "@/types/Food";
import type Table from "@/types/Table";
import ConfirmDialog from "@/components/ConfirmDialog.vue";

const confirmDlg = ref();

export const useOrderStore = defineStore("Order", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();

  let tableIds = 0;
  const dialog = ref(false);
  const orders = ref<Order[]>([]);
  const orderLists = ref<Order[]>([]);
  // const finshedList = ref<orderItem[]>([]);
  // const selectedList = ref<orderItem[]>([]);
  const orderItemList = ref<orderItem[]>([]);

  const orderList = ref<{ food: Food; count: number; sum: number }[]>([]);

  async function getOrders() {
    try {
      const res = await orderService.getOrders();
      orderLists.value = res.data;
      for (const i in orderLists.value) {
        for (const j in orderLists.value[i].orderItems) {
          orderItemList.value.push(orderLists.value[i].orderItems[j]);
        }
      }
    } catch (e) {
      console.log(e);
      messageStore.showMessage("Can't get order information from database.");
    }
  }

  const deleteOrder = (id: number): void => {
    const index = orderLists.value.findIndex((item) => item.id === id);
    orderLists.value.splice(index, 1);
  };

  //เพิ่มรายการอาหาร
  const addFood = (item: Food) => {
    for (let i = 0; i < orderList.value.length; i++) {
      if (orderList.value[i].food.id === item.id) {
        orderList.value[i].count++;
        orderList.value[i].sum = orderList.value[i].count * item.price;
        return;
      }
    }
    orderList.value.push({ food: item, count: 1, sum: 1 * item.price });
  };

  //ลบรายอาหาร
  const deleteFood = (index: number) => {
    orderList.value.splice(index, 1);
  };

  //ราคารวม
  const sumPrice = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].sum;
    }
    return sum;
  });

  //จำนวนรวม
  const sumCount = computed(() => {
    let sum = 0;
    for (let i = 0; i < orderList.value.length; i++) {
      sum = sum + orderList.value[i].count;
    }
    return sum;
  });

  async function openOrder() {
    loadingStore.isLoading = true;
    const orderItem = orderList.value.map(
      (item) =>
        <{ foodId: number; qty: number }>{
          foodId: item.food.id,
          qty: item.count,
        }
    );
    // console.log(orderItems);
    const order = { tableId: tableIds, orderItems: orderItem };
    console.log(order);
    try {
      const res = await orderService.saveOrder(order);
      dialog.value = false;
      clearOrder();
    } catch (e) {
      // messageStore.showError("ไม่สามารถบันทึก Order ได้");
    }
    loadingStore.isLoading = false;
  }

  function clearOrder() {
    orderList.value = [];
  }

  function getTableId(id: number) {
    tableIds = id;
    console.log(tableIds);
    return tableIds;
  }

  // const deleteOrder = (id: number): void => {
  //   const index = orderList.value.findIndex((item) => item.id === id);
  //   orderList.value.splice(index, 1);
  // };

  // const finshedOrder = (id: number): void => {
  //   //const index = orderList.value.findIndex((item) => item.id === id);
  //   const selectedindex = selectedList.value.findIndex(
  //     (item) => item.id === id
  //   );
  //   finshedList.value.push(selectedList.value[selectedindex]);
  //   selectedList.value.splice(selectedindex, 1);
  // };

  // const selectedOrder = (id: number): void => {
  //   const index = orderList.value.findIndex((item) => item.id === id);
  //   selectedList.value.push(orderItemList.value[index]);
  //   deleteOrder(id);
  // };

  return {
    orders,
    dialog,
    addFood,
    // finshedOrder,
    deleteFood,
    orderList,
    sumPrice,
    sumCount,
    openOrder,
    getOrders,
    orderLists,
    deleteOrder,
    getTableId,
    clearOrder,
    tableIds,
  };
});
