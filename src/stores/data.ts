import { ref } from "vue";
import { defineStore } from "pinia";
import type Data from "@/types/Data";

export const useDataStore = defineStore("data", () => {
  const dialog = ref(false);
  const isTable = ref(true);
  const editedData = ref<Data>({ id: -1, name: "", price: "", img: "" });
  const datas = ref<Data[]>([
    {
      id: 1,
      name: "ข้าวขาหมู",
      price: "50",
      img: "https://goodlifeupdate.com/app/uploads/2017/05/%E0%B8%82%E0%B9%89%E0%B8%B2%E0%B8%A7%E0%B8%82%E0%B8%B2%E0%B8%AB%E0%B8%A1%E0%B8%B9-%E0%B8%94%E0%B8%B4-%E0%B9%80%E0%B8%AD%E0%B8%A1%E0%B9%80%E0%B8%A1%E0%B8%AD%E0%B8%A3%E0%B8%B1%E0%B8%A5%E0%B8%94%E0%B9%8C-02.jpg",
    },
    {
      id: 2,
      name: "ข้าวมันไก่",
      price: "45",
      img: "https://static.thairath.co.th/media/4DQpjUtzLUwmJZZSEmAUm74bI2EL8Sb34rOSLQkKjXQF.jpg",
    },
    {
      id: 3,
      name: "ข้าวกะเพรา",
      price: "40",
      img: "https://d1sag4ddilekf6.azureedge.net/compressed/merchants/3-CZDHMAJGTYXZUA/hero/e19bbb4cea4a49818b225c49ddd09ae1_1586758089451885089.png",
    },
    {
      id: 4,
      name: "ข้าวเปล่า",
      price: "10",
      img: "https://yayoirestaurants.com/productimages/8712_SD001_Japanese-Rice.jpg",
    },
    {
      id: 5,
      name: "ข้าวไข่ดาว",
      price: "30",
      img: "https://i.pinimg.com/originals/ea/4d/5e/ea4d5e439b3790527ab79b096012118f.jpg",
    },
  ]);

  const Drinkdatas = ref<Data[]>([
    {
      id: 1,
      name: "นํ้าเปล่า",
      price: "20",
      img: "https://st.bigc-cs.com/public/media/catalog/product/47/88/8851952350147/8851952350147.jpg",
    },
    {
      id: 2,
      name: "เป๊ปซี่",
      price: "30",
      img: "https://backend.tops.co.th/media/catalog/product/8/8/8858998581214_30-07-2021.jpg",
    },
    {
      id: 3,
      name: "โค้ก",
      price: "30",
      img: "https://backend.tops.co.th/media/catalog/product/8/8/8851959149010_04-03-2022.jpg",
    },
    {
      id: 4,
      name: "ชาเขียวเย็น",
      price: "40",
      img: "https://img.wongnai.com/p/400x0/2020/11/09/7587f2a134e74562beab5f1d215c15db.jpg",
    },
    {
      id: 5,
      name: "โกโก้เย็น",
      price: "40",
      img: "https://weetacoffeehome.files.wordpress.com/2019/02/15.png",
    },
  ]);

  const Dessertdatas = ref<Data[]>([
    {
      id: 1,
      name: "ทับทิมกรอบ",
      price: "40",
      img: "https://www.ryoiireview.com/upload/article/201610/1477556698_799bad5a3b514f096e69bbc4a7896cd9.jpg",
    },
    {
      id: 2,
      name: "ลอดช่อง",
      price: "40",
      img: "https://www.easycookingmenu.com/media/k2/items/cache/041f8ef5b15effcb1dc4f41914f9679e_XL.jpg",
    },
    {
      id: 3,
      name: "สละลอยแก้ว",
      price: "30",
      img: "https://food.mthai.com/app/uploads/2017/06/Salacca-zalacca-in-sweet-syrup1.jpg",
    },
    {
      id: 4,
      name: "ไอศกรีมกะทิ",
      price: "40",
      img: "https://www.thaismescenter.com/wp-content/uploads/2017/10/wedfghnhgfd.jpg",
    },
    {
      id: 5,
      name: "ขนมถ้วย",
      price: "40",
      img: "https://howtocookhub.com/wp-content/uploads/2022/08/%E0%B8%82%E0%B8%99%E0%B8%A1%E0%B8%96%E0%B9%89%E0%B8%A7%E0%B8%A2.jpg",
    },
  ]);

  return { datas, Drinkdatas, Dessertdatas };
});
