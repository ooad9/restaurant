import { ref, watch } from "vue";
import { defineStore } from "pinia";
import type Food from "@/types/Food";
import foodService from "@/services/food";

export const useFoodStore = defineStore("food", () => {
  const foodTypes = ref(["ทั้งหมด", "อาหาร", "ของหวาน", "เครื่องดื่ม"]);
  const selectedFoodType = ref(foodTypes.value[0]);
  const foods = ref<Food[]>([]);
  const foodsByType = ref<Food[]>([]);

  watch(selectedFoodType, () => {
    getFoodsByType();
  });

  async function getFoods() {
    try {
      const res = await foodService.getFoods();
      foods.value = res.data;
      if (selectedFoodType.value === "ทั้งหมด") {
        foodsByType.value = foods.value;
      }
    } catch (e) {
      console.log(e);
    }
  }

  async function getFoodsByType() {
    if (selectedFoodType.value === "ทั้งหมด") {
      await getFoods();
    } else {
      try {
        const res = await foodService.getType(selectedFoodType.value);
        foodsByType.value = res.data;
      } catch (e) {
        console.log(e);
      }
    }
  }

  return {
    foods,
    foodTypes,
    getFoods,
    getFoodsByType,
    foodsByType,
    selectedFoodType,
  };
});
