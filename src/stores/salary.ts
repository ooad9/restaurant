import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type Salary from "@/types/Salary";
import SalaryService from "@/services/salary";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useSalaryStore = defineStore("Salary", () => {
  const loadingStore = useLoadingStore();
  //const messageStore = useMessageStore();
  const dialog = ref(false);
  const salarys = ref<Salary[]>([]);
  const editedSalary = ref<Salary>({
    date: "",
    payCircle: "",
    hours: 0,
    total: 0,
  });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedSalary.value = { date: "", payCircle: "", hours: 0, total: 0 };
    }
  });
  async function getSalarys() {
    loadingStore.isLoading = true;
    try {
      const res = await SalaryService.getSalarys();
      salarys.value = res.data;
    } catch (e) {
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveSalary() {
    loadingStore.isLoading = true;
    try {
      if (editedSalary.value.id) {
        const res = await SalaryService.updateSalary(
          editedSalary.value.id,
          editedSalary.value
        );
      } else {
        const res = await SalaryService.saveSalary(editedSalary.value);
      }

      dialog.value = false;
      await getSalarys();
    } catch (e) {
      //messageStore.showError("ไม่สามารถบันทึก Salary ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteSalary(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await SalaryService.deleteSalary(id);
      await getSalarys();
    } catch (e) {
      console.log(e);
      //messageStore.showError("ไม่สามารถลบ Salary ได้");
    }
    loadingStore.isLoading = false;
  }
  function editSalary(Salary: Salary) {
    editedSalary.value = JSON.parse(JSON.stringify(Salary));
    dialog.value = true;
  }
  return {
    salarys,
    getSalarys,
    dialog,
    editedSalary,
    saveSalary,
    editSalary,
    deleteSalary,
  };
});
