import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/employee",
      name: "employee",
      component: () => import("../views/employee/EmployeeView.vue"),
    },
    {
      path: "/user",
      name: "user",
      component: () => import("../views/user/UserView.vue"),
    },
    {
      path: "/tableEmployee",
      name: "tableEmployee",
      component: () => import("../views/table/TableEmployeeView.vue"),
    },
    {
      path: "/tableCustomer",
      name: "tableCustomer",
      component: () => import("../views/table/TableCustomerView.vue"),
    },

    {
      path: "/order",
      name: "order",
      component: () => import("../views/order/OrderView.vue"),
    },
    {
      path: "/cooking",
      name: "cooking",
      component: () => import("../views/cooking/CookingView.vue"),
    },
    {
      path: "/serve",
      name: "serve",
      component: () => import("../views/serve/ServeView.vue"),
    },
    {
      path: "/bill",
      name: "bill",
      component: () => import("../views/bill/BillView.vue"),
    },
    {
      path: "/salary",
      name: "salary",
      component: () => import("../views/salary/SalaryView.vue"),
    },
  ],
});

export default router;
