import type Order from "@/types/Order";
import http from "./axios";
function getOrders() {
  return http.get("/orders");
}

function saveOrder(order: { orderItems: { foodId: number; qty: number }[] }) {
  return http.post("/orders", order);
}

function updateOrder(id: number, order: Order) {
  return http.patch("/orders/" + id, order);
}

function deleteOrder(id: number) {
  return http.delete("/orders/" + id);
}

export default { getOrders, saveOrder, updateOrder, deleteOrder };
