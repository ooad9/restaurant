import type Food from "@/types/Food";
import http from "./axios";
function getFoods() {
  return http.get("foods");
}

function getFoodByID(id: number) {
  return http.get(`/foods/${id}`);
}

function getType(type: string) {
  return http.get(`/foods/type/${type}`);
}
export default {
  getFoods,
  getType,
  getFoodByID,
};
