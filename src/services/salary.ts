import type Salary from "@/types/Salary";
import http from "./axios";
function getSalarys() {
  return http.get("/salarys");
}

function saveSalary(Salary: Salary) {
  return http.post("/salarys", Salary);
}

function updateSalary(id: number, Salary: Salary) {
  return http.patch(`/salarys/${id}`, Salary);
}

function deleteSalary(id: number) {
  return http.delete(`/salarys/${id}`);
}

export default { getSalarys, saveSalary, updateSalary, deleteSalary };
