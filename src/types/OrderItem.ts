import type Food from "./Food";
import type order from "./Order";

export default interface orderItem {
  id?: number;
  name?: string;
  qty: number;
  order: order;
  product: Food;
}
