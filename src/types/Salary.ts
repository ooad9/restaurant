export default interface Salary {
  id?: number;
  date: string;
  payCircle: string;
  hours: number;
  total: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
