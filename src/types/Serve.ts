export default interface Serve {
  id: number;
  queue: number;
  table: number;
  menu: string;
  qty: number;
  status: string;
}
