import type orderItem from "./OrderItem";
import type Table from "./Table";

export default interface Order {
  id?: number;
  table_number?: number;
  table: Table;
  orderItems: orderItem[];

  // name: string;
  // qty: number;
  // status: string;

  timestampOpen?: Date;
  timestampClose?: Date;
  deletedAt?: Date;
}
