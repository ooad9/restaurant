export default interface Bill {
  id: number;
  queue: number;
  table: number;
  menu: string;
  qty: number;
  price: number;
}
