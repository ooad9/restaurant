export default interface Employee {
  id?: number;
  firstname: string;
  lastname: string;
  username: string;
  password: string;
  age: number;
  phone: string;
  position: string;
}
