export default interface Food {
  id: number;
  name: string;
  type: string;
  img: string;
  price: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
