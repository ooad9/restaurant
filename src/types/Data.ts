export default interface Data {
  id: number;
  name: string;
  price: string;
  img: string;
}
