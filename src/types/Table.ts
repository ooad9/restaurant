export default interface Table {
  id: number;
  name?: string;
  amount?: number;
  status?: string;

  createdAt?: Date;
  UpdatedAt?: Date;
  deleteddAt?: Date;
}
